// 選択中スプレッドシートの全データを取得
function GetActiveSheetAllRange() {
    var ss = SpreadsheetApp.getActiveSpreadsheet().getActiveSheet();

    // getRange(開始行番号,開始列番号,行数,列数)
    var data = ss.getRange(1, 1, ss.getLastRow(), ss.getLastColumn()).getValues();

    return data;

  }

// 選択中スプレッドシートの全データを取得（A1列基準）
function GetActiveSheetDataForDeliveryPlan() {
    var ss = SpreadsheetApp.getActiveSpreadsheet().getActiveSheet();

    // A1セルを基準に最終行を取得
    var LastRow = GetLastRow(1);

    // getRange(開始行番号,開始列番号,行数,列数)
    var data = ss.getRange(2, 1, LastRow, ss.getLastColumn()).getValues();

    return data;

  }

function GetLastRow(pColPosition){
    var sheet = SpreadsheetApp.getActiveSpreadsheet().getActiveSheet();

    //A列の最終行のセルから[Ctrl + ↑]
    var lastRow2 = sheet.getRange(sheet.getMaxRows(), pColPosition).getNextDataCell(SpreadsheetApp.Direction.UP).getRow();

    return lastRow2;
}

function GetLastColBy(pRowPosition){
    var ss = SpreadsheetApp.getActiveSpreadsheet();
    var sheet = ss.getRange(pPosition);

    //1行目の最終列のセルから[Ctrl + ←]
   var lastCol2 = sheet.getRange(pRowPosition, sheet.getMaxColumns()).getNextDataCell(SpreadsheetApp.Direction.PREVIOUS).getColumn();

    return lastCol2;
}
