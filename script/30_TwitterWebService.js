// ------------------------------------------------------------
// Mark: Twitter API
// ------------------------------------------------------------

/* あらかじめアプリ登録して取得 */
var API_KEY = PropertiesService.getScriptProperties().getProperty('TWITTER_API_KEY');
var API_SECRET = PropertiesService.getScriptProperties().getProperty('TWITTER_API_SECRET');

/* サービスの設定 */
function getService() {
  return OAuth1.createService('Twitter')
      .setAccessTokenUrl('https://api.twitter.com/oauth/access_token')
      .setRequestTokenUrl('https://api.twitter.com/oauth/request_token')
      .setAuthorizationUrl('https://api.twitter.com/oauth/authorize')
      .setConsumerKey(API_KEY)
      .setConsumerSecret(API_SECRET)
      .setCallbackFunction('authCallback')
      .setPropertyStore(PropertiesService.getUserProperties());
}

/* コールバック関数 */
function authCallback(request) {
  var service = getService();
  var authorized = service.handleCallback(request);
  if (authorized) return HtmlService.createHtmlOutput('認証成功');
}

/* 認証リセット */
function reset() {
  getService().reset();
}

/* 認証用URL */
function getOAuthURL() {
  Logger.log(getService().authorize());
}

/* リクエスト */
function TwitterRequest(pScreenName) {
  var service = getService();
  if (service.hasAccess()) {
    var url = 'https://api.twitter.com/1.1/users/show.json?screen_name=' + pScreenName;
    var options = {
      method: 'GET',
      escaping: false
    };

    try {
        var response = service.fetch(url, options);
        var result = JSON.parse(response.getContentText());
//        Logger.log(JSON.stringify(result, null, 2));
        return result;
      } catch(e) {
        return "-";
      }
  }
}

/* Twitterへポスト */
function TwitterPost(pPostText, pPostID) {
  var service = getService();
  if (service.hasAccess()) {
    var url = 'https://api.twitter.com/1.1/statuses/update.json';

    // post内容をセット
    if (pPostID == undefined) {
      var payload = {
        status: pPostText
      }
    } else {
      var payload = {
        status: pPostText,
        in_reply_to_status_id: pPostID
      };
    };

    var options = {
      method: 'POST',
      payload: payload
    };

    // 投稿
    try {
      var response = service.fetch(url, options);
      var result = JSON.parse(response.getContentText());
      return result;
    } catch(e) {
      // 失敗
      return e;
    }
  }
}

