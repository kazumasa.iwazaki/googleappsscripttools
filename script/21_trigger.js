/* トリガーから起動するときの処理 */
function execFromTrigger() {

  // 過去分のトリガーを削除
  deleteTrigger()

  // Twitterのフォロワー数取得
  postTweet()

}

/* 時分まで指定したトリガーをセット（当日内に実施するもののみ） */
function setTriggerAt18() {
  setTrigger(18);
}

function setTriggerAt06() {
  setTrigger(06);
}

function setTrigger(pExecHour) {
  var triggerDay = new Date();
  triggerDay.setHours(pExecHour);
  triggerDay.setMinutes(00);
  ScriptApp.newTrigger("execFromTrigger").timeBased().at(triggerDay).create();
}

/* トリガーを削除する関数(消さないと残る) */
function deleteTrigger() {
  var triggers = ScriptApp.getProjectTriggers();
  for(var i=0; i < triggers.length; i++) {
    if (triggers[i].getHandlerFunction() == "execFromTrigger") {
      ScriptApp.deleteTrigger(triggers[i]);
    }
  }
}
