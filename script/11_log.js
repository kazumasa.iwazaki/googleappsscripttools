function setNewLog(pLogInfo) {
  // 更新対象シートをセット
  var targetSheetName = "ログ"
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sh = ss.getSheetByName(targetSheetName)
  var data = sh.getRange(1, 1, sh.getLastRow(), sh.getLastColumn()).getValues();

  var lastRow = data.length + 1;
  // 対象判定用の日時を作成
  var targetDate = new Date();

  // 最終行を取得してログを書き出し
  sh.getRange(lastRow, 1).setValue(Utilities.formatDate(targetDate, 'Asia/Tokyo', 'yyyy/MM/dd HH:mm:ss'));
  sh.getRange(lastRow, 2).setValue(pLogInfo);

}

function setAddLog(pLogInfo, pColPosition) {
  // 更新対象シートをセット
  var targetSheetName = "ログ"
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sh = ss.getSheetByName(targetSheetName)
  var data = sh.getRange(1, 1, sh.getLastRow(), sh.getLastColumn()).getValues();

  // 最終行の指定カラムへ更新
  var lastRow = data.length;
  sh.getRange(lastRow, pColPosition).setValue(pLogInfo);
}
