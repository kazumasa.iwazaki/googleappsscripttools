function test() {
  postTweet(new Date('2019/01/03 18:00:00'));
}

// ------------------------------------------------------------
// Mark: メイン処理
// ------------------------------------------------------------
/* Twitterへ投稿 */
function postTweet(pBaseDate) {
  // 引数がある場合は、その日付を基準に処理を実施（デバッグ用）
  if (pBaseDate) {
    var baseDate = pBaseDate;
  } else {
    var baseDate = new Date();
  }

  // 対象となるイベントを取得
  var eventArray = getTargetEvent(baseDate);
  Logger.log('eventArray');
  Logger.log(eventArray);

  // イベントがある場合のみポスト
  if (eventArray.length > 0) {
    // 日付順にソート（元の配列もソートされる）
    var sortedArray = eventArray.sort();
    Logger.log('sortedArray');
    Logger.log(sortedArray);

    // １投稿単位にまとめる
    var postTextArray = makePostText(sortedArray, 140);
    Logger.log('postTextArray');
    Logger.log(postTextArray);

    // Twitterへポスト（戻り値は使用する予定はない）
    var result = doPostText(postTextArray);
  } else {
    setNewLog('投稿対象なし')
  }

}


// ------------------------------------------------------------
// Mark: function
// ------------------------------------------------------------
/* 対象のイベントを抽出して配列として返す */
function getTargetEvent(pBaseDate) {
  // 更新対象シートをセット
  var targetSheetName = "アンケート結果"
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sh = ss.getSheetByName(targetSheetName)
  var data = sh.getRange(1, 1, sh.getLastRow(), sh.getLastColumn()).getValues();

  // 現在日時を元に対象となる日付範囲をセット(現在日時〜翌日の6:00）
  var rangeDateFrom = new Date(pBaseDate);
  var rangeDateTo = new Date(pBaseDate);
  rangeDateTo.setDate(rangeDateTo.getDate() + 1);
  rangeDateTo.setHours(06);
  rangeDateTo.setMinutes(00);
  rangeDateTo.setSeconds(00);

  // 基準時間を元に対象時間をセット
  if (Utilities.formatDate(pBaseDate, 'Asia/Tokyo', 'HH') > 12) {
      // 18時に実行 ⇒ 18:00〜翌6:00
      var judgeBaseTime = 12;
   } else {
     // 6時に実行 ⇒ 6:00〜翌6:00
     var judgeBaseTime = 24;
   }

  // 対象日付の範囲内であれば配列へ追加
  var lastRow = data.length - 1;
  var eventArray = [];
  for (var i = 1; i <= lastRow; i++) {
    // 先頭行が空文字を目印として処理を終了させる
    if (data[0][0] == '') {break};

    // 対象判定用の日時を作成
    var targetDate = new Date(data[i][2]);
    targetDate.setHours(Utilities.formatDate(data[i][3], 'Asia/Tokyo', 'HH'));

    // 終了時間を基準として対象日時を取得、配列へセット
    var jadgeRange = (rangeDateTo.getTime() - targetDate.getTime()) / (1000 * 60 * 60);
    if (jadgeRange <= judgeBaseTime && jadgeRange >= 0) {
      eventArray.push([Utilities.formatDate(data[i][2], 'Asia/Tokyo', 'MM/dd'), Utilities.formatDate(data[i][3], 'Asia/Tokyo', 'HH:mm'), data[i][1], data[i][4]]);
    }
  }

  return eventArray;
}

/* 一回の投稿に合わせて配列を作成 */
function makePostText(pSortedArray, pLength) {
  var sortedArrayCount = pSortedArray.length
  var postTextArray = [];

  // ヘッダー情報を埋め込んでおく
  var wkString = '【今後のスケジュール】\n';

  // 最大文字列を超過するまで文字列を結合、超過する直前(１回の文字数上限直前)を配列へセット
  for (var i = 0; i <= sortedArrayCount - 1; i++) {
    var result = wkString.concat(editArray(pSortedArray[i]));
    if (result.length > pLength) {
      postTextArray.push(wkString);
      wkString = editArray(pSortedArray[i]);
    } else {
      wkString = wkString.concat(editArray(pSortedArray[i]));
    };
  };

  // 最後にURLとハッシュタグを追加
  var targetSheetName = "フッター情報"
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sh = ss.getSheetByName(targetSheetName);
  var data = sh.getRange(1, 1, sh.getLastRow(), sh.getLastColumn()).getValues();

  if (data.length　> 1) {
    var url = data[1][0];
  } else {
    var url = '';
  };

  // ハッシュタグを作成
  var hashTag = '';
  var lastRow = data.length - 1
  for (var i = 1; i <= lastRow; i++) {
    if (data[i][1] == '') {break};

    if (i > 1) {
      hashTag = hashTag.concat(' ');
     };
    hashTag = hashTag.concat('#' + data[i][1]);
  }

  // フッター情報を含めて文字数超過有無を判断、配列へセット
  var result = wkString.concat('\n', url, '\n', hashTag);
  if (result.length > pLength) {
    postTextArray.push(wkString);
    postTextArray.push('\n' + url + '\n' + hashTag);
  } else {
    postTextArray.push(result);
  };

  return postTextArray;
}

function editArray(pArray) {
  var result = pArray[2] + '  ' + pArray[0] + '  ' + pArray[1] + '  ' + pArray[3] + '\n';
  return result;
}

/* 投稿処理 */
function doPostText(pPostTextArray) {
  var arrayCount = pPostTextArray.length;

  // 投稿
  for (i = 0; i <= arrayCount - 1; i++ ) {
    if (i == 0) {
      // 初回の投稿、IDを取得しておく
      var result = TwitterPost(pPostTextArray[i]);
      if (result.id_str != undefined) {
        setNewLog('post ok!');
        var postID = result.id_str;
      } else {
        setNewLog(result.message);
        return '-';
      };
    } else {
      // 一回で投稿しきれない場合はリプライとして投稿
      var result = TwitterPost(pPostTextArray[i], postID);
      if (result.id_str != undefined) {
        setAddLog('post ok!', i + 2);
        var postID = result.id_str;
      } else {
        setAddLog(result, i + 2);
        return '-';
      };
    }
  }
}
